package cache

type Config struct {
	Address  string `env:"CACHE_ADDRESS"`
	Password string `env:"CACHE_PASSWORD"`
	Port     string `env:"CACHE_PORT"`
}
