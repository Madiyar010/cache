package cache

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-redis/redismock/v9"
	"github.com/redis/go-redis/v9"
	"io"
	"testing"
	"time"
)

func TestRedisCache_Delete(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "1"
	mock.ExpectDel(key).SetVal(0)
	err := mockRedisCache.Delete(context.Background(), false, key)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}

func TestRedisCache_Delete2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "2"
	mock.ExpectScan(0, key, 0).SetVal([]string{}, 0)
	err := mockRedisCache.Delete(context.Background(), true, key)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Delete2_2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "2"
	mock.ExpectScan(0, key, 0).SetErr(errors.New("1"))
	err := mockRedisCache.Delete(context.Background(), true, key)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Delete2_3(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "2"
	mock.ExpectScan(0, key, 0).SetVal([]string{"1", "2"}, 0)
	mock.ExpectDel("1", "2").SetErr(errors.New("1"))
	err := mockRedisCache.Delete(context.Background(), true, key)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Delete3(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "2"
	mock.ExpectScan(0, key, 0).SetVal([]string{"2", "3"}, 0)
	mock.ExpectDel("2", "3").SetVal(0)
	err := mockRedisCache.Delete(context.Background(), true, key)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Delete4(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "1"
	mock.ExpectDel(key).SetErr(errors.New("ExpectDel"))
	err := mockRedisCache.Delete(context.Background(), false, key)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Get(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetVal("0")
	err := mockRedisCache.Get(context.Background(), key, nil)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}

}

func TestRedisCache_Get2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetErr(errors.New("1"))
	err := mockRedisCache.Get(context.Background(), key, nil)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}

}
func TestRedisCache_Get2_2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetErr(redis.Nil)
	err := mockRedisCache.Get(context.Background(), key, nil)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}

}
func TestRedisCache_Get2_3(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetErr(errors.New("1"))
	err := mockRedisCache.Get(context.Background(), key, nil)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}

}
func TestRedisCache_Set(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectSet(key, key, 1*time.Second).SetVal("0")
	err := mockRedisCache.Set(context.Background(), key, nil, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}

func TestRedisCache_Set2(t *testing.T) {
	var data []byte
	db, mock := redismock.NewClientMock()
	ch := make(chan Error)
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  ch,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"

	go func() {
		<-ch
	}()
	mock.ExpectSet(key, key, 1*time.Second).SetErr(errors.New("1"))
	err := mockRedisCache.Set(context.Background(), key, data, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Set3(t *testing.T) {
	data := []byte{'1'} // Передаем строку, а не массив байтов
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"

	mock.ExpectSet(key, data, 1*time.Second).SetVal("1") // Здесь мы также передаем строку, а не ключ
	err := mockRedisCache.Set(context.Background(), key, []byte(data), 1*time.Second)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}

type MyJSONDecoder2 struct{}

func (d *MyJSONDecoder2) Decode(r io.Reader, val interface{}) error {
	// Имитация корректной остановки сервера
	return nil
}

func (d *MyJSONDecoder2) Encode(w io.Writer, value interface{}) error {
	// Имитация корректной остановки сервера
	return errors.New("1")
}
func TestRedisCache_Set4(t *testing.T) {
	var data bool // Передаем строку, а не массив байтов
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  make(chan Error),
		broadcasting: true,
		decoder:      &MyJSONDecoder2{},
	}
	key := "1"
	go func() {
		<-mockRedisCache.cacheErrors
	}()
	mock.ExpectSet(key, data, 1*time.Second).SetVal("1") // Здесь мы также передаем строку, а не ключ
	err := mockRedisCache.Set(context.Background(), key, data, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Expire(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectExpire(key, 1*time.Second).SetErr(errors.New("1"))
	err := mockRedisCache.Expire(context.Background(), key, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Expire2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectExpire(key, 1*time.Second).SetErr(errors.New("1"))
	err := mockRedisCache.Expire(context.Background(), key, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Expire3(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectExpire(key, 1*time.Second).SetVal(true)
	err := mockRedisCache.Expire(context.Background(), key, 1*time.Second)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}

type MyJSONDecoder struct{}

func (d *MyJSONDecoder) Decode(r io.Reader, val interface{}) error {
	// Имитация корректной остановки сервера
	return nil
}

func (d *MyJSONDecoder) Encode(w io.Writer, value interface{}) error {
	// Имитация корректной остановки сервера
	return nil
}

func TestRedisCache_broadcastError(t *testing.T) {
	// Создаем мок клиента Redis
	db, _ := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  make(chan Error),
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}

	// Определяем ошибку для тестирования
	errRedis := &redisError{
		err:     errors.New("1"),
		command: "err",
	}

	// Вызываем метод broadcastError с тестовой ошибкой
	go func() {
		mockRedisCache.broadcastError(errRedis)
		close(mockRedisCache.cacheErrors) // Закрываем канал после отправки ошибки
	}()

	// Проверяем, что ошибка получена из канала
	select {
	case errFromChannel := <-mockRedisCache.cacheErrors:
		if errFromChannel != errRedis {
			t.Errorf("Received error %v, expected %v", errFromChannel, errRedis)
		}
	case <-time.After(1 * time.Second):
		t.Error("Did not receive error within 1 second")
	}
}
func TestRedisError_Error(t *testing.T) {
	test := &redisError{
		err:     nil,
		command: "123",
	}
	r := test.Error()
	if r == fmt.Sprintf("redis: ошибка %s произошла при попытке выполнить команду %s", test.err, test.command) {
		t.Errorf("func (r *redisError) Error() string")
	}
}
func TestRedisError_GetCommand(t *testing.T) {
	test := &redisError{
		err:     nil,
		command: "123",
	}
	r := test.GetCommand()
	if r != string(test.command) {
		t.Errorf("func TestRedisError_GetCommand(t *testing.T)")
	}
}
func TestRedisError_KeyExists(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetErr(redis.Nil)
	b, err := mockRedisCache.KeyExists(context.Background(), key)
	if b != false || err != ErrCacheMiss {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisError_KeyExists2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	nerr := errors.New("0")
	mock.ExpectGet(key).SetErr(nerr)
	b, err := mockRedisCache.KeyExists(context.Background(), key)
	if b != true || err != nerr {
		t.Errorf("Ошибка: %v", err)
	}
}
