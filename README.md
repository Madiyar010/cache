# Cache

Пакет cache предоставляет абстракцию для работы с кэшем, которая позволяет легко заменять и мокать реализации кэша в приложении.

## Установка

```shell
go get "gitlab.com/golight/cache"
```


## Пример использования

```go
package main

import (
	"context"
	"gitlab.com/golight/cache"
	"log"
	"time"
)

func main() {
	conf:= cache.Config{
		Address:  "cache",
		Password: "cache",
		Port:     "cache",
	}
	// Создание кэша с помощью пакета cache
	c, err := cache.NewCache(conf, nil)
	if err != nil {
		log.Fatalf("Не удалось создать кэш: %v", err)
	}

	// Установка значения в кэш
	err = c.Set(context.Background(), "my_key", "some value", time.Minute)
	if err != nil {
		log.Printf("Не удалось установить значение: %v", err)
	}

	// Извлечение значения из кэша
	var val string
	err = c.Get(context.Background(), "my_key", &val)
	if err != nil {
		log.Printf("Не удалось получить значение: %v", err)
	}

	// Удаление значения из кэша
	err = c.Delete(context.Background(), false, "my_key")
	if err != nil {
		log.Printf("Не удалось удалить значение: %v", err)
	}
}



```