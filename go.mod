module gitlab.com/golight/cache

go 1.19

require (
	github.com/go-redis/redismock/v9 v9.2.0
	github.com/ptflp/godecoder v0.0.1
	github.com/redis/go-redis/v9 v9.4.0
	github.com/stretchr/testify v1.3.0
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
