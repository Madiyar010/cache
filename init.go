package cache

import (
	"context"
	"fmt"
	"github.com/ptflp/godecoder"
	"github.com/redis/go-redis/v9"
	"time"
)

// NewCache принимает конфигурацию и в зависимости от раздела кэша в конфигурации оборачивает фактическую реализацию кэша
func NewCache(cacheConfig Config, decoder godecoder.Decoder) (Cacher, error) {
	redisCache, err := newRedisCache(fmt.Sprintf("%s:%s", cacheConfig.Address, cacheConfig.Port), cacheConfig.Password, decoder)
	if err != nil {
		return nil, err
	}

	return redisCache, nil
}
func newRedisCache(address, password string, decoder godecoder.Decoder) (*RedisCache, error) {
	client := redis.NewClient(&redis.Options{
		Addr:            address,
		Password:        password,
		ConnMaxIdleTime: 55 * time.Second,
	})
	ctx := context.Background()

	err := client.Ping(ctx).Err()
	if err == nil {
		redisCache := &RedisCache{rdb: client, decoder: decoder}
		return redisCache, nil
	}

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	timeoutExceeded := time.After(time.Second * time.Duration(defaultReconnectionTimeout))

	for {
		select {

		case <-timeoutExceeded:
			return nil, fmt.Errorf("cache connection failed after %d timeout", defaultReconnectionTimeout)

		case <-ticker.C:
			err := client.Ping(ctx).Err()
			if err == nil {
				return &RedisCache{rdb: client, decoder: decoder}, nil
			}
		}
	}
}
